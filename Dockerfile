FROM node:0.10.36

RUN mkdir -p /usr/src/app \
  && npm install -g gulp bower strongloop
WORKDIR /usr/src/app

ONBUILD COPY . /usr/src/app/
ONBUILD RUN npm install --unsafe-perm

CMD [ "slc", "run" ]
